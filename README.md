# Catalyst IT Submission

This script iterates through the provided csv, validates the data, and inserts the rows into the users table.

## Table of Contents

- [Requirements](#requirements)
- [Usage](#usage)
- [Contact](#contact)

## Requirements

1. An existing database named 'catalyst' must be present on the database server
2. The 'mysqli' extension must be enabled in php.ini.
3. Tested with PHP 7.4.15
4. The csv fields must be in the following order: name, surname, email

## Usage
<pre>
--create_table      Creates the 'users' database table. Will DROP any existing 'users' tables.  
--help              Prints the help menu.  
--file              Specifies the csv file to use. Not required for --create_table or --help.  
--dry_run           Can be used with --file to provide an output of test run data. Data will not be inserted.  
-u                  Required. The database user to connect with.  
-p                  Required. The database user's password.  
-h                  Required. The hostname or IP address of the database server.  
</pre>
## Contact

Author: Matt Price  
Email: pricem1990@gmail.com  
Mobile: 0432 365 152  