<?php
    for($i = 1; $i <= 100; $i++){
        $foo = !boolval($i % 3);
        $bar = !boolval($i % 5);

        if($foo || $bar) {
            $result = ($foo) ? "foo" : "";
            $result .= ($bar) ? "bar" : "";
        } else {
            $result = $i;
        }

        $result .= ($i != 100) ? ", " : "";
        echo($result);
    }