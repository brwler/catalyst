<?php
    $args = getopt("u:p:h:file:", ["dry_run", "create_table", "help", "file:"]);
    $database = null;

    if(isset($args['help'])){//Print help menu
        printHelp();
        escape($database);
    }

    try {
        //Ensure database connection can be made
        $required = ['u', 'p', 'h'];

        foreach($required as $req){
            if(!array_key_exists($req, $args)){
                throw new Exception("Missing database connection flag: -" . $req);
            }
        }

        $database = getConnection($args['h'], $args['u'], $args['p']);

        //Create Table and exit
        if (isset($args['create_table'])) {
            createTable($database);
            escape($database);
        }

        //File flag required below
        if(!isset($args['file'])){
            throw new Exception("--file flag must be set");
        }

        //Ensure file exists and is .csv; Load data into array
        if(file_exists($args['file'])){
            $info = pathinfo($args['file']);
            if($info['extension'] != 'csv'){
                throw new Exception("Error with file: '" . $args['file'] . "' must be .csv format.");
            }
            $users = array_map( 'str_getcsv', file($args['file']));
        } else {
            throw new Exception("Error with file: '" . $args['file'] . "' not found.");
        }

        $query = "
            INSERT INTO users
            (name, surname, email)
            VALUES (?,?,?)";
        $statement = $database->prepare($query);

        //Loop through csv rows; validate data; echo if dry_run flag present, otherwise insert into db
        foreach($users as $user){
            $name = validateName($user[0]);
            $surname = validateName($user[1]);
            $email = validateEmail($user[2]);

            if ($email != "") {
                if (isset($args['dry_run'])) {
                    $query = str_replace('?', "%s", $query);
                    $query = sprintf($query, $name, $surname, $email);
                    print_r("Test Run: " . $query . PHP_EOL);
                } else {
                    $statement->bind_param("sss", $name, $surname, $email);
                    $statement->execute();
                }
            } else {
                print_r(sprintf("Not inserting: %s %s - Invalid email format: %s" . PHP_EOL, $name, $surname, $user[2]));
            }
        }
    } catch (Exception $e){
        echo($e->getMessage());
    }
    escape($database);

    /**
     * Get a connection to a database
     * @param string $host Database hostname (e.g.: 192.168.0.100, db.domain.com)
     * @param string $user Database username
     * @param string $password Database Password
     * @param string $dbName Database Name (Defaults to "catalyst")
     * @param int $port Database Port (Defaults to 3306)
     * @return mysqli
     */
    function getConnection(string $host, string $user, string $password, string $dbName = "catalyst", int $port = 3306): mysqli
    {
        $conn = null;
        try {
            $conn = new mysqli($host, $user, $password, $dbName, $port);
            if ($conn->connect_errno) {
                throw new Exception($conn->connect_error);
            }
        } catch (Exception $e){
            echo("Error connecting to the database: " . $e->getMessage());
        }
        return $conn;
    }

    /**
     * Prints the help menu
     */
    function printHelp(){
        $options = [
            '--create_table' => "Creates the 'users' database table. Will DROP any existing 'users' tables.",
            '--help' => "Prints this help menu.",
            '--file' => "Specifies the csv file to use. Not required for --create_table or --help.",
            '--dry_run' => "Can be used with --file to provide an output of test run data. Data will not be inserted.",
            '-u' => "Required. The database user to connect with.",
            '-p' => "Required. The database user's password.",
            '-h' => "Required. The hostname or IP address of the database server"];

        $result = "--==Help Menu==-- \nThis script iterates through the provided csv, validates the data, and inserts the rows into the users table.\n\n";
        $result .= "--==Requirements==--\n1. An existing database named 'catalyst' must be present.\n2. Extension 'mysqli' must be enabled in php.ini.\n\n";
        $result .= "--==Options==--\n\n";

        foreach($options as $key => $value){
            $result .= str_pad($key, 20) . $value . PHP_EOL;
        }

        echo($result);
    }

    /**
     * Verify string is correctly formatted email; Empty string on failure
     * @param string $email The email to be sanitized
     * @return string|null The sanitized email; Empty string on failure
     */
    function validateEmail(string $email): ?string
    {
        return (filter_var($email, FILTER_VALIDATE_EMAIL)) ? strtolower($email) : "";
    }

    /**
     * Capitalize the first letter (including after an apostrophe) of the provided name
     * @param string $name The name to be sanitised
     * @return string The sanitised name
     */
    function validateName(string $name): string
    {
        return str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtolower($name))));
    }

    /**
     * Create the database table for user upload
     * @param mysqli $db
     */
    function createTable(mysqli $db){
        $dropQuery = "DROP TABLE IF EXISTS users";
        $statement = $db->prepare($dropQuery);
        $statement->execute();

        $createQuery = "
            CREATE TABLE users (
            name VARCHAR(255) NULL,
            surname VARCHAR(255) NULL,
            email VARCHAR(255) NOT NULL,
            PRIMARY KEY (email),
            UNIQUE INDEX email_UNIQUE (email ASC));";
        $statement = $db->prepare($createQuery);
        $statement->execute();
    }

    /**
     * Gracefully exit the program
     * @param $db (mysqli) database object to be closed or null
     */
    function escape($db): void
    {
        if($db instanceof mysqli) {
            $db->close();
        }
        exit();
    }